---
layout: article
title:  "How to Use Git From Inside Emacs"
blurb: "Magit is an Emacs-based Git client which makes Git usable quickly and efficiently."
date: 2020-11-10 17:29:33
categories: tutorials git emacs
image: /assets/2020-11-10-using-magit-for-emacs/magit-status.png
---
Magit is the best way to use Git when developing in Emacs. It integrates the primary Git workflow into the text editor and provides sensible keybinds by default.

#### installation
To install Magit, you first need to add MELPA by adding following snippet to your `config.el` file. (if you have installed any packages in EMACS, you likely already have MELPA).

``` emacs-lisp
(require 'package)
(add-to-list 'package-archives
    '("melpa" . "http://melpa.org/packages/") t)
```

Next, install Magit with the following keyboard sequences:
``` emacs-lisp
;; Refresh the package list
M-x package-refresh-contents RET

;; Install Magit
M-x package-install RET magit RET
```

#### using Magit
To access Magit, use `M-x magit-status` while viewing a file in a git repository. You will be greeted by a window similar to this one:

![Magit Screenshot](/assets/2020-11-10-using-magit-for-emacs/magit-status.png)

It is called the **status window**, and it will be where you interact with Magit the most.

To get started, look at the list of **unstaged files**. These files represent changes you made since you last updated, or **commited** your files. To add files to the next commit (called **staging**), you should place your cursor over the filename you want to add and press the `s` key. The same can be done for the **untracked files** list. To stage all the files in one of these 2 lists, press `s` while on it's heading.

Now, you will want to **commit** your files. To do so, press the `c` key twice while in the status window. Then, type in a description for what your commit does. Finally, use `C-c C-c` to save the commit. If you want to cancel it, use `C-c C-k`.

If you have your repository hosted on GitLab or a similar service, you will want to be able to add your commits to it. In order to do so, you must **push** them. In Magit, you can do so by typing `Pp` (push parent) or `Pu` (push upstream) in the status window. This will push any new changes to your online repository.

If you use Git with multiple machines/people, you will need to **pull** down new changes. To do so, use the `Fp` (fetch parent) or `Fu` (fetch upstream) keybinds.

#### conclusion
Now, you should be able to navigate yourself around Magit and use Git. I hope you enjoyed reading this. I would like to once again mention our [new Matrix chat](https://matrix.to/#/+nilptr:matrix.org), where you can ask questions and give feedback on our articles, in addition to getting notified of new NilPtr articles. Thank you for reading!
