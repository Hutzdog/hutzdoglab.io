---
layout: article
title:  "Rc: A minimal *nix shell."
blurb: "The Rc shell is a minimal and well thought out shell for *nix systems."
date: 2020-11-07 10:13:49
categories: showcases rc
image: /assets/2020-11-07-intro-to-rc-shell/googleimg.png
---
The Rc shell is a *nix shell developed for Plan9 and Version 10 Unix. It is minimal and well thought-out, yet is a great daily driver for those who find the other shells bloated.

#### why Rc
Firstly, the syntax of Rc is reasonable. In Bash, the `if` statement alone has strict spacing requirements, a unique closing syntax (`fi` is only used in `if` statements), and in general are very verbose (requiring a `then` statement on it's own line). In contrast, the Rc `if` statement uses parentheses and brackets, is not obsessed with spacing, and is quite compact. Here is a comparison of the 2:
```shell
# RC
if (true) {
    echo "This will run"
}
if not {
    echo "This will not"
}

# Bash
if [[ true ]]
then
    echo "This will run"
else
    echo "This will not"
fi
```

On top of that, the Rc shell is much more minimal. By default, it's prompt is just a semicolon (which allows for a user to copy a line and paste it without worrying about copying part of their prompt) and must be configured to your liking (by editing the `.rcrc` file). This gives you a shell that you work best in, without features you don't need.

#### installing Rc
There are several builds of Rc floating around out there. I recommend the Unix port by Byron Rakitziz, as it supports line editing and tab completions. It can be installed using the Nix package manager (we will assume a non-NixOS install here for portability reasons) via the following command:

``` shell
nix-env -iA nixpkgs.rc
```

Now, we need to create a startup script and add it to our shells. This is because Rc only reads it's configuration file if it is a login shell. We also will manually source .rcrc, as some implementations of Rc read from `$home/lib/profile` instead. Run these commands as a user with sudo privileges. (These commands only work in Posix shells due to differences in interpolation of HereDocs)/

``` shell
bash
cat >> ./rcl << END
#!/home/$USER/.nix-profile/bin/rc
# Rc startup script

. \$/.rcrc
path=(\$DOLLAR^path /home/$USER/.nix-profile/bin/)
rc -l
END

chmod +x ./rcl
sudo mv ./rcl /usr/bin/
sudo echo /usr/bin/rcl >> /etc/shells
exit
```

Finally, you need to change your login shell. To do this, run the following command as your user. Note that the long shebang is just because your login shell is not configured yet. Scripts that run under Rc can use a shebang of `#!/usr/bin/env rc`.

``` shell
chsh -s /usr/bin/rcl
```

Now, whenever you login, you will be using Rc.

#### configuring Rc
To configure Rc, create/edit a file in your home directory called `.rcrc`. It should contain anything you would like to do on startup (for example, defining the prompt and path variables) and any functions you want to have everywhere. I recommend reading the man pages for Rc (you can find out how to set them up [here](/posts/man-pages-on-windows/)) to find out what exactly is available to configure.

#### conclusion
Rc is a very interesting and minimal shell. It is cleaner than the POSIX family while still being somewhat familiar to Bash users.

#### further reading
[https://github.com/rakitzis/rc]() - The port's GitHub page

[http://doc.cat-v.org/plan_9/4th_edition/papers/rc]() - An official paper on the shell. It covers many of it's features
