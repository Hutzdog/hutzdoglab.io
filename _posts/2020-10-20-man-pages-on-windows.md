---
layout: article
title:  "Setting up Man Pages(including Windows)"
blurb: "Man pages are an offline way to learn about how programs work."
date:   2020-10-20 2:20:00
categories: tutorials development
image: /assets/2020-10-05-carp-lang-showcase/googleimage.png
---
Man pages are among the most useful sources of information about programs. They are accessed via the `man` command and are only available on *nix/Mac systems. However, through a little trickery they can be used on Windows.

#### installing `man`
Mac - `man` is already installed by default on Macs.

Linux/BSD - `man` is probably already installed. If not, consult your package manager

Windows - On Windows, the install process is a little more involved
1. Install the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10). Unless you already know how to use Linux, I recommend choosing Ubuntu as your distribution, as this guide is designed around it.
2. Open up Ubuntu in WSL and type the following commands
``` shell
sudo apt install manpages-dev manpages-possix-dev most
echo -e "export PAGER=\"most\"" >> ~/.bashrc
source ~/.bashrc
```
3. Test the `man` command by running the following command
``` shell
man man
```

4. Add an alias in PowerShell so you can use `man` from there. Write this in `\Users\<USERNAME>\Documents\PowerShell\Microsoft.PowerShell_profile.ps1`

```powershell
Remove-Alias -Name man

function man([string]$page)
{
    wsl bash -ic "man $page"
}
```

You should now have `man` installed.

#### additional software
There are a few additional packages which give `man` even more features!
- [stdman](https://github.com/jeaye/stdman) - Man pages for the C++ standard library.
- [tldr](https://tldr.sh) - Cross-platform alternative to `man`, useful if you need Windows command documentation or want a general abstract of a command.

#### conclusion
Overall, `man` pages are a very useful tool for figuring out how stuff on your computer works. Thank you for reading!
