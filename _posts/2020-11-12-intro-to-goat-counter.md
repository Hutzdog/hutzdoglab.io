---
layout: article
title:  "GoatCounter: Privacy Respecting Analytics"
blurb: "GoatCounter is an analytics platform which respects user privacy, while not costing non-commercial users!"
date: 2020-11-12 17:58:29
categories: showcases goatcounter
image: /assets/2020-11-12-intro-to-goat-counter/googleimage.png
---
Every site who wishes to grow needs some form of analytics. However, the biggest player, Google Analytics, uses the data it collects to power it's targeted ad platform. While privacy-respecting options exist, even nonprofits have to shell out a subscription in order to protect their users. GoatCounter changes that, as it provides free service to any "non commercial" website.

#### why should you care
GoatCounter provides a way for budding websites and side projects to figure out what the average user wants without paying with their data (GoatCounter doesn't even need a GDPR notice!) or the admin's wallet. And, if the site takes off, you still have one of the best implemented analytics platforms behind you (provided you pay for the starter plan once your endeavor becomes profitable)!

that's not enough, GoatCounter adds much less bulk to your site in comparison to Google Analytics. It also is open-source, allowing you to host your own instance of it and see exactly what data your site is collecting.

#### conclusion
GoatCounter is an excellent option for your budding website, side project, or even Google Analytics-driven website. Check out their [live demo](https://stats.arp242.net) and their [project's website](https://goatcounter.com) for more info. Thank you for reading!
