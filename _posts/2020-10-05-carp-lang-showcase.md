---
layout: article
title:  "Introducing Carp: A Rusty Lisp"
blurb: "Carp is a new language combining the performance of Rust with the power of LISP."
date:   2020-10-05 20:17:40
categories: showcases carp
image: /assets/2020-10-05-carp-lang-showcase/googleimage.png
---
Carp is an interesting little language. A combination of Rust's memory management and LISP's syntax, Carp seems like the language for anyone wanting to use LISP in a high-performance environment. Carp is currently on version 0.3 and is considered an early alpha. I can see it eventually growing to compete with the likes of Nim and Crystal: bringing a higher level syntax to a lower level language. However, because LISP was designed in such a way as to be easily parsed, I can see it compiling faster than C while nearly matching it's run speed. It draws a lot of inspiration from Clojure's syntax, so it's syntax highlighting should work the best as a base or band-aid solution.

Here is the example program on their GitHub page:

{% highlight clojure %}
(load-and-use SDL)

(defn tick [state]
  (+ state 10))

(defn draw [app rend state]
  (bg rend &(rgb (/ @state 2) (/ @state 3) (/ @state 4))))

(defn main []
  (let [app (SDLApp.create "The Minimalistic Color Generator" 400 300)
        state 0]
    (SDLApp.run-with-callbacks &app SDLApp.quit-on-esc tick draw state)))
{% endhighlight %}

As you can see, it has integrated SDL bindings. You can download it [here](https://github.com/carp-lang/Carp), so go give a little known language with massive potential some love!.
