---
layout: article
title:  "Where I've Been"
blurb: "Why I haven't posted recently"
date: 2020-12-10 11:06:18
categories: announcements other
---
Hi guys,
I just wanted to provide a small status update on why I haven't been posting. Quite frankly, life has been a bit stressful and I have had other things I needed to focus on. I'll try to post when I can, however I may not be able to keep up with daily posts. I don't have much else to write, so I'll see you guys when I write my next article.
