---
layout: article
title:  "A Few Announcements"
blurb: "A few announcements about the future of nilptr.dev."
date: 2020-11-09 17:50:21
categories: announcements other
---
Hi guys! As you may know, this blog has had a few weeks to get started, and over the past few days I have been working on consolidating a plan on how to progress past the initial stage. In light of that, I have a few announcements.

#### the new view counter
As you may have noticed, the site now uses a per-article view counter. If you have not, that would mean that you have an ad-blocker enabled. Due to circumstances beyond my control and the nature of static site generators, I have to make use of an external script for it. I have audited the source code for the counter and can confirm it does no additional tracking of the user outside of counting that there was a visitor to the page in order for me to determine what content the community wants the most. I will continue to strive to maintain transparency on the technologies used on this blog.

#### more regular posting
I have decided that I would like to try to get an upload schedule set up for this blog. I am hoping to aim for 1 post on every weekday (Monday-Friday). Writing this blog has been quite fun over the past little while and I hope that having a schedule will help ensure I keep producing content for your enjoyment.

#### matrix community
In order to have some degree of reader engagement with these articles, I have created a Matrix community. Matrix is an open source decentralized communication platform which is free to use. You can sign up for it using the [element web client](https://element.io) and join our new community by clicking [here](https://matrix.to/#/+nilptr:matrix.org).

#### conclusion
Please feel free to leave any feedback on Matrix. I hope that these revisions will help improve the experience for NilPtr viewers. Thank you for reading!
