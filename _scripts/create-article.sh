#!/bin/sh
cd "$(dirname $0)"
if [ $# -lt 1 ]; then
    echo "ERROR> No filename provided"
    exit 1
fi

TEMPLATE="article-template.md"
FNAME="$(date +"%Y-%m-%d")-$1"

cp $TEMPLATE "$FNAME.md"
mkdir ../assets/$FNAME

sed -i "s/<DATE>/$(date +"%Y-%m-%d %T")/" "$FNAME.md"
sed -i "s/<SEIMG>/\/assets\/$FNAME\/googleimage.png/" "$FNAME.md"

mv "$FNAME.md" ../_posts/
