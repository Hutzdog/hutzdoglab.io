---
layout: default
title: about
permalink: /about/
---

# about

I am Daniel Hutzley, a high school student concurrently enrolled in a C++ Computer Science course. I usually work with Rust and Zig, though I can live in most language if needs be.

NilPtr is my blog about technology and programming. I hope to make a weekly entry here about some topic in FOSS, programming, and/or new tools I have worked with.

#### technologies
Here is a list of the technologies used on this blog:
* [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages)
* [Jekyll](https://jekyllrb.com/)
* [Wing CSS](https://kbrsh.github.io/wing)
* [Simple Jekyll Search](https://github.com/christian-fei/Simple-Jekyll-Search)
* [GoatCounter](https://github.com/zygoat/goatcounter<br)<br

#### community
We now have a Matrix community! Come join us [here](https://matrix.to/#/+nilptr:matrix.org)!
